/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "detalle_ven")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleVen.findAll", query = "SELECT d FROM DetalleVen d"),
    @NamedQuery(name = "DetalleVen.findByTotal", query = "SELECT d FROM DetalleVen d WHERE d.total = :total"),
    @NamedQuery(name = "DetalleVen.findByDescripcion", query = "SELECT d FROM DetalleVen d WHERE d.descripcion = :descripcion"),
    @NamedQuery(name = "DetalleVen.findByIva", query = "SELECT d FROM DetalleVen d WHERE d.iva = :iva"),
    @NamedQuery(name = "DetalleVen.findByCantidad", query = "SELECT d FROM DetalleVen d WHERE d.cantidad = :cantidad"),
    @NamedQuery(name = "DetalleVen.findByRefDetalleVenta", query = "SELECT d FROM DetalleVen d WHERE d.refDetalleVenta = :refDetalleVenta"),
    @NamedQuery(name = "DetalleVen.findByFormadepago", query = "SELECT d FROM DetalleVen d WHERE d.formadepago = :formadepago")})
public class DetalleVen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Total")
    private int total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IVA")
    private int iva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cantidad")
    private int cantidad;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref_Detalle_Venta")
    private Integer refDetalleVenta;
    @Size(max = 50)
    @Column(name = "Forma de pago")
    private String formadepago;
    @JoinColumn(name = "Fk_Ref_Fact_Ven", referencedColumnName = "Ref_Fact_Ven")
    @ManyToOne(optional = false)
    private FactVen fkRefFactVen;

    public DetalleVen() {
    }

    public DetalleVen(Integer refDetalleVenta) {
        this.refDetalleVenta = refDetalleVenta;
    }

    public DetalleVen(Integer refDetalleVenta, int total, String descripcion, int iva, int cantidad) {
        this.refDetalleVenta = refDetalleVenta;
        this.total = total;
        this.descripcion = descripcion;
        this.iva = iva;
        this.cantidad = cantidad;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIva() {
        return iva;
    }

    public void setIva(int iva) {
        this.iva = iva;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getRefDetalleVenta() {
        return refDetalleVenta;
    }

    public void setRefDetalleVenta(Integer refDetalleVenta) {
        this.refDetalleVenta = refDetalleVenta;
    }

    public String getFormadepago() {
        return formadepago;
    }

    public void setFormadepago(String formadepago) {
        this.formadepago = formadepago;
    }

    public FactVen getFkRefFactVen() {
        return fkRefFactVen;
    }

    public void setFkRefFactVen(FactVen fkRefFactVen) {
        this.fkRefFactVen = fkRefFactVen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refDetalleVenta != null ? refDetalleVenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleVen)) {
            return false;
        }
        DetalleVen other = (DetalleVen) object;
        if ((this.refDetalleVenta == null && other.refDetalleVenta != null) || (this.refDetalleVenta != null && !this.refDetalleVenta.equals(other.refDetalleVenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.DetalleVen[ refDetalleVenta=" + refDetalleVenta + " ]";
    }
    
}
