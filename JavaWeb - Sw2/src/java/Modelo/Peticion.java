/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "peticion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Peticion.findAll", query = "SELECT p FROM Peticion p"),
    @NamedQuery(name = "Peticion.findByRefPedido", query = "SELECT p FROM Peticion p WHERE p.refPedido = :refPedido"),
    @NamedQuery(name = "Peticion.findByFecha", query = "SELECT p FROM Peticion p WHERE p.fecha = :fecha")})
public class Peticion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref_Pedido")
    private Integer refPedido;
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @JoinColumn(name = "Fk_Id_Usuario", referencedColumnName = "Id_Usuario")
    @ManyToOne(optional = false)
    private Usuario fkIdUsuario;

    public Peticion() {
    }

    public Peticion(Integer refPedido) {
        this.refPedido = refPedido;
    }

    public Integer getRefPedido() {
        return refPedido;
    }

    public void setRefPedido(Integer refPedido) {
        this.refPedido = refPedido;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario getFkIdUsuario() {
        return fkIdUsuario;
    }

    public void setFkIdUsuario(Usuario fkIdUsuario) {
        this.fkIdUsuario = fkIdUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refPedido != null ? refPedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Peticion)) {
            return false;
        }
        Peticion other = (Peticion) object;
        if ((this.refPedido == null && other.refPedido != null) || (this.refPedido != null && !this.refPedido.equals(other.refPedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Peticion[ refPedido=" + refPedido + " ]";
    }
    
}
