/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "punto_geografico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PuntoGeografico.findAll", query = "SELECT p FROM PuntoGeografico p"),
    @NamedQuery(name = "PuntoGeografico.findByIdPunto", query = "SELECT p FROM PuntoGeografico p WHERE p.idPunto = :idPunto"),
    @NamedQuery(name = "PuntoGeografico.findByDescripcionPunto", query = "SELECT p FROM PuntoGeografico p WHERE p.descripcionPunto = :descripcionPunto"),
    @NamedQuery(name = "PuntoGeografico.findByNombre", query = "SELECT p FROM PuntoGeografico p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "PuntoGeografico.findBySector", query = "SELECT p FROM PuntoGeografico p WHERE p.sector = :sector")})
public class PuntoGeografico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_Punto")
    private Integer idPunto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Descripcion_Punto")
    private String descripcionPunto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Sector")
    private int sector;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkIdPunto")
    private Collection<Usuario> usuarioCollection;

    public PuntoGeografico() {
    }

    public PuntoGeografico(Integer idPunto) {
        this.idPunto = idPunto;
    }

    public PuntoGeografico(Integer idPunto, String descripcionPunto, String nombre, int sector) {
        this.idPunto = idPunto;
        this.descripcionPunto = descripcionPunto;
        this.nombre = nombre;
        this.sector = sector;
    }

    public Integer getIdPunto() {
        return idPunto;
    }

    public void setIdPunto(Integer idPunto) {
        this.idPunto = idPunto;
    }

    public String getDescripcionPunto() {
        return descripcionPunto;
    }

    public void setDescripcionPunto(String descripcionPunto) {
        this.descripcionPunto = descripcionPunto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPunto != null ? idPunto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PuntoGeografico)) {
            return false;
        }
        PuntoGeografico other = (PuntoGeografico) object;
        if ((this.idPunto == null && other.idPunto != null) || (this.idPunto != null && !this.idPunto.equals(other.idPunto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.PuntoGeografico[ idPunto=" + idPunto + " ]";
    }
    
}
