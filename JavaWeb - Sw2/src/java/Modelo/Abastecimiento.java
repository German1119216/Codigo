/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "abastecimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Abastecimiento.findAll", query = "SELECT a FROM Abastecimiento a"),
    @NamedQuery(name = "Abastecimiento.findByNit", query = "SELECT a FROM Abastecimiento a WHERE a.nit = :nit"),
    @NamedQuery(name = "Abastecimiento.findByNombreEmp", query = "SELECT a FROM Abastecimiento a WHERE a.nombreEmp = :nombreEmp")})
public class Abastecimiento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Nit")
    private Integer nit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Nombre_Emp")
    private int nombreEmp;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkIdDistribuidor")
    private Collection<FactCom> factComCollection;

    public Abastecimiento() {
    }

    public Abastecimiento(Integer nit) {
        this.nit = nit;
    }

    public Abastecimiento(Integer nit, int nombreEmp) {
        this.nit = nit;
        this.nombreEmp = nombreEmp;
    }

    public Integer getNit() {
        return nit;
    }

    public void setNit(Integer nit) {
        this.nit = nit;
    }

    public int getNombreEmp() {
        return nombreEmp;
    }

    public void setNombreEmp(int nombreEmp) {
        this.nombreEmp = nombreEmp;
    }

    @XmlTransient
    public Collection<FactCom> getFactComCollection() {
        return factComCollection;
    }

    public void setFactComCollection(Collection<FactCom> factComCollection) {
        this.factComCollection = factComCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nit != null ? nit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Abastecimiento)) {
            return false;
        }
        Abastecimiento other = (Abastecimiento) object;
        if ((this.nit == null && other.nit != null) || (this.nit != null && !this.nit.equals(other.nit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Abastecimiento[ nit=" + nit + " ]";
    }
    
}
