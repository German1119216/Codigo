/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "fact_com")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FactCom.findAll", query = "SELECT f FROM FactCom f"),
    @NamedQuery(name = "FactCom.findByRefFactCom", query = "SELECT f FROM FactCom f WHERE f.refFactCom = :refFactCom"),
    @NamedQuery(name = "FactCom.findByFechaPedAbaste", query = "SELECT f FROM FactCom f WHERE f.fechaPedAbaste = :fechaPedAbaste")})
public class FactCom implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref_Fact_Com")
    private Integer refFactCom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Ped_Abaste")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPedAbaste;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "Pago")
    private byte[] pago;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkFactCompra")
    private Collection<DetalleCom> detalleComCollection;
    @JoinColumn(name = "Fk_Id_Emp", referencedColumnName = "C")
    @ManyToOne(optional = false)
    private Empleado fkIdEmp;
    @JoinColumn(name = "Fk_Id_Distribuidor", referencedColumnName = "Nit")
    @ManyToOne(optional = false)
    private Abastecimiento fkIdDistribuidor;

    public FactCom() {
    }

    public FactCom(Integer refFactCom) {
        this.refFactCom = refFactCom;
    }

    public FactCom(Integer refFactCom, Date fechaPedAbaste, byte[] pago) {
        this.refFactCom = refFactCom;
        this.fechaPedAbaste = fechaPedAbaste;
        this.pago = pago;
    }

    public Integer getRefFactCom() {
        return refFactCom;
    }

    public void setRefFactCom(Integer refFactCom) {
        this.refFactCom = refFactCom;
    }

    public Date getFechaPedAbaste() {
        return fechaPedAbaste;
    }

    public void setFechaPedAbaste(Date fechaPedAbaste) {
        this.fechaPedAbaste = fechaPedAbaste;
    }

    public byte[] getPago() {
        return pago;
    }

    public void setPago(byte[] pago) {
        this.pago = pago;
    }

    @XmlTransient
    public Collection<DetalleCom> getDetalleComCollection() {
        return detalleComCollection;
    }

    public void setDetalleComCollection(Collection<DetalleCom> detalleComCollection) {
        this.detalleComCollection = detalleComCollection;
    }

    public Empleado getFkIdEmp() {
        return fkIdEmp;
    }

    public void setFkIdEmp(Empleado fkIdEmp) {
        this.fkIdEmp = fkIdEmp;
    }

    public Abastecimiento getFkIdDistribuidor() {
        return fkIdDistribuidor;
    }

    public void setFkIdDistribuidor(Abastecimiento fkIdDistribuidor) {
        this.fkIdDistribuidor = fkIdDistribuidor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refFactCom != null ? refFactCom.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactCom)) {
            return false;
        }
        FactCom other = (FactCom) object;
        if ((this.refFactCom == null && other.refFactCom != null) || (this.refFactCom != null && !this.refFactCom.equals(other.refFactCom))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.FactCom[ refFactCom=" + refFactCom + " ]";
    }
    
}
