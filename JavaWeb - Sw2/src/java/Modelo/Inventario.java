/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "inventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventario.findAll", query = "SELECT i FROM Inventario i"),
    @NamedQuery(name = "Inventario.findByStock", query = "SELECT i FROM Inventario i WHERE i.stock = :stock"),
    @NamedQuery(name = "Inventario.findByFkPresentacion", query = "SELECT i FROM Inventario i WHERE i.fkPresentacion = :fkPresentacion"),
    @NamedQuery(name = "Inventario.findByEntradas", query = "SELECT i FROM Inventario i WHERE i.entradas = :entradas"),
    @NamedQuery(name = "Inventario.findBySalidas", query = "SELECT i FROM Inventario i WHERE i.salidas = :salidas"),
    @NamedQuery(name = "Inventario.findByFechaEntrada", query = "SELECT i FROM Inventario i WHERE i.fechaEntrada = :fechaEntrada"),
    @NamedQuery(name = "Inventario.findByIdInventario", query = "SELECT i FROM Inventario i WHERE i.idInventario = :idInventario")})
public class Inventario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Stock")
    private int stock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fk_Presentacion")
    private int fkPresentacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Entradas")
    private int entradas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Salidas")
    private int salidas;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Entrada")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrada;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_Inventario")
    private Integer idInventario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkIdInventario")
    private Collection<PresentacionProducto> presentacionProductoCollection;

    public Inventario() {
    }

    public Inventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Inventario(Integer idInventario, int stock, int fkPresentacion, int entradas, int salidas, Date fechaEntrada) {
        this.idInventario = idInventario;
        this.stock = stock;
        this.fkPresentacion = fkPresentacion;
        this.entradas = entradas;
        this.salidas = salidas;
        this.fechaEntrada = fechaEntrada;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getFkPresentacion() {
        return fkPresentacion;
    }

    public void setFkPresentacion(int fkPresentacion) {
        this.fkPresentacion = fkPresentacion;
    }

    public int getEntradas() {
        return entradas;
    }

    public void setEntradas(int entradas) {
        this.entradas = entradas;
    }

    public int getSalidas() {
        return salidas;
    }

    public void setSalidas(int salidas) {
        this.salidas = salidas;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    @XmlTransient
    public Collection<PresentacionProducto> getPresentacionProductoCollection() {
        return presentacionProductoCollection;
    }

    public void setPresentacionProductoCollection(Collection<PresentacionProducto> presentacionProductoCollection) {
        this.presentacionProductoCollection = presentacionProductoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInventario != null ? idInventario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario)) {
            return false;
        }
        Inventario other = (Inventario) object;
        if ((this.idInventario == null && other.idInventario != null) || (this.idInventario != null && !this.idInventario.equals(other.idInventario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Inventario[ idInventario=" + idInventario + " ]";
    }
    
}
