/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "detalle_com")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleCom.findAll", query = "SELECT d FROM DetalleCom d"),
    @NamedQuery(name = "DetalleCom.findByTotal", query = "SELECT d FROM DetalleCom d WHERE d.total = :total"),
    @NamedQuery(name = "DetalleCom.findByDescripcion", query = "SELECT d FROM DetalleCom d WHERE d.descripcion = :descripcion"),
    @NamedQuery(name = "DetalleCom.findByIva", query = "SELECT d FROM DetalleCom d WHERE d.iva = :iva"),
    @NamedQuery(name = "DetalleCom.findByCantidad", query = "SELECT d FROM DetalleCom d WHERE d.cantidad = :cantidad"),
    @NamedQuery(name = "DetalleCom.findByIdDetalleCompra", query = "SELECT d FROM DetalleCom d WHERE d.idDetalleCompra = :idDetalleCompra")})
public class DetalleCom implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Total")
    private int total;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IVA")
    private int iva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cantidad")
    private int cantidad;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_Detalle_Compra")
    private Integer idDetalleCompra;
    @JoinColumn(name = "Fk_Fact_Compra", referencedColumnName = "Ref_Fact_Com")
    @ManyToOne(optional = false)
    private FactCom fkFactCompra;

    public DetalleCom() {
    }

    public DetalleCom(Integer idDetalleCompra) {
        this.idDetalleCompra = idDetalleCompra;
    }

    public DetalleCom(Integer idDetalleCompra, int total, String descripcion, int iva, int cantidad) {
        this.idDetalleCompra = idDetalleCompra;
        this.total = total;
        this.descripcion = descripcion;
        this.iva = iva;
        this.cantidad = cantidad;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIva() {
        return iva;
    }

    public void setIva(int iva) {
        this.iva = iva;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getIdDetalleCompra() {
        return idDetalleCompra;
    }

    public void setIdDetalleCompra(Integer idDetalleCompra) {
        this.idDetalleCompra = idDetalleCompra;
    }

    public FactCom getFkFactCompra() {
        return fkFactCompra;
    }

    public void setFkFactCompra(FactCom fkFactCompra) {
        this.fkFactCompra = fkFactCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleCompra != null ? idDetalleCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleCom)) {
            return false;
        }
        DetalleCom other = (DetalleCom) object;
        if ((this.idDetalleCompra == null && other.idDetalleCompra != null) || (this.idDetalleCompra != null && !this.idDetalleCompra.equals(other.idDetalleCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.DetalleCom[ idDetalleCompra=" + idDetalleCompra + " ]";
    }
    
}
