/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
    @NamedQuery(name = "Producto.findByRef", query = "SELECT p FROM Producto p WHERE p.ref = :ref"),
    @NamedQuery(name = "Producto.findByCaracteristica", query = "SELECT p FROM Producto p WHERE p.caracteristica = :caracteristica")})
public class Producto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref")
    private Integer ref;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Caracteristica")
    private String caracteristica;
    @OneToMany(mappedBy = "fkProducto")
    private Collection<PresentacionProducto> presentacionProductoCollection;

    public Producto() {
    }

    public Producto(Integer ref) {
        this.ref = ref;
    }

    public Producto(Integer ref, String caracteristica) {
        this.ref = ref;
        this.caracteristica = caracteristica;
    }

    public Integer getRef() {
        return ref;
    }

    public void setRef(Integer ref) {
        this.ref = ref;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    @XmlTransient
    public Collection<PresentacionProducto> getPresentacionProductoCollection() {
        return presentacionProductoCollection;
    }

    public void setPresentacionProductoCollection(Collection<PresentacionProducto> presentacionProductoCollection) {
        this.presentacionProductoCollection = presentacionProductoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ref != null ? ref.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.ref == null && other.ref != null) || (this.ref != null && !this.ref.equals(other.ref))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Producto[ ref=" + ref + " ]";
    }
    
}
