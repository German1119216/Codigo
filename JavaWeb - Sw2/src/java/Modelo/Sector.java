/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "sector")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sector.findAll", query = "SELECT s FROM Sector s"),
    @NamedQuery(name = "Sector.findByRefSector", query = "SELECT s FROM Sector s WHERE s.refSector = :refSector"),
    @NamedQuery(name = "Sector.findByDescripcionSector", query = "SELECT s FROM Sector s WHERE s.descripcionSector = :descripcionSector")})
public class Sector implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref_Sector")
    private Integer refSector;
    @Size(max = 50)
    @Column(name = "Descripcion_Sector")
    private String descripcionSector;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sector")
    private Collection<Empleado> empleadoCollection;

    public Sector() {
    }

    public Sector(Integer refSector) {
        this.refSector = refSector;
    }

    public Integer getRefSector() {
        return refSector;
    }

    public void setRefSector(Integer refSector) {
        this.refSector = refSector;
    }

    public String getDescripcionSector() {
        return descripcionSector;
    }

    public void setDescripcionSector(String descripcionSector) {
        this.descripcionSector = descripcionSector;
    }

    @XmlTransient
    public Collection<Empleado> getEmpleadoCollection() {
        return empleadoCollection;
    }

    public void setEmpleadoCollection(Collection<Empleado> empleadoCollection) {
        this.empleadoCollection = empleadoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refSector != null ? refSector.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sector)) {
            return false;
        }
        Sector other = (Sector) object;
        if ((this.refSector == null && other.refSector != null) || (this.refSector != null && !this.refSector.equals(other.refSector))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Sector[ refSector=" + refSector + " ]";
    }
    
}
