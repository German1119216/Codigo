/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findByC", query = "SELECT e FROM Empleado e WHERE e.c = :c"),
    @NamedQuery(name = "Empleado.findByDireccion", query = "SELECT e FROM Empleado e WHERE e.direccion = :direccion")})
public class Empleado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "C")
    private Integer c;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "Turno")
    private byte[] turno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "Direccion")
    private String direccion;
    @JoinColumn(name = "Fk_Id_Persona", referencedColumnName = "C")
    @ManyToOne(optional = false)
    private Persona fkIdPersona;
    @JoinColumn(name = "Sector", referencedColumnName = "Ref_Sector")
    @ManyToOne(optional = false)
    private Sector sector;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkIdEmp")
    private Collection<FactCom> factComCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fKIdEmp")
    private Collection<FactVen> factVenCollection;

    public Empleado() {
    }

    public Empleado(Integer c) {
        this.c = c;
    }

    public Empleado(Integer c, byte[] turno, String direccion) {
        this.c = c;
        this.turno = turno;
        this.direccion = direccion;
    }

    public Integer getC() {
        return c;
    }

    public void setC(Integer c) {
        this.c = c;
    }

    public byte[] getTurno() {
        return turno;
    }

    public void setTurno(byte[] turno) {
        this.turno = turno;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Persona getFkIdPersona() {
        return fkIdPersona;
    }

    public void setFkIdPersona(Persona fkIdPersona) {
        this.fkIdPersona = fkIdPersona;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    @XmlTransient
    public Collection<FactCom> getFactComCollection() {
        return factComCollection;
    }

    public void setFactComCollection(Collection<FactCom> factComCollection) {
        this.factComCollection = factComCollection;
    }

    @XmlTransient
    public Collection<FactVen> getFactVenCollection() {
        return factVenCollection;
    }

    public void setFactVenCollection(Collection<FactVen> factVenCollection) {
        this.factVenCollection = factVenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (c != null ? c.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.c == null && other.c != null) || (this.c != null && !this.c.equals(other.c))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Empleado[ c=" + c + " ]";
    }
    
}
