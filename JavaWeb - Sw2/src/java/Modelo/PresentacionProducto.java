/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "presentacion_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PresentacionProducto.findAll", query = "SELECT p FROM PresentacionProducto p"),
    @NamedQuery(name = "PresentacionProducto.findByRefCilindro", query = "SELECT p FROM PresentacionProducto p WHERE p.refCilindro = :refCilindro"),
    @NamedQuery(name = "PresentacionProducto.findByPrecio", query = "SELECT p FROM PresentacionProducto p WHERE p.precio = :precio"),
    @NamedQuery(name = "PresentacionProducto.findByTipo", query = "SELECT p FROM PresentacionProducto p WHERE p.tipo = :tipo")})
public class PresentacionProducto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ref_Cilindro")
    private Integer refCilindro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio")
    private double precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tipo")
    private int tipo;
    @JoinColumn(name = "Fk_Producto", referencedColumnName = "Ref")
    @ManyToOne
    private Producto fkProducto;
    @JoinColumn(name = "Fk_Id_Inventario", referencedColumnName = "id_Inventario")
    @ManyToOne(optional = false)
    private Inventario fkIdInventario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkPresentacionProducto")
    private Collection<FactVen> factVenCollection;

    public PresentacionProducto() {
    }

    public PresentacionProducto(Integer refCilindro) {
        this.refCilindro = refCilindro;
    }

    public PresentacionProducto(Integer refCilindro, double precio, int tipo) {
        this.refCilindro = refCilindro;
        this.precio = precio;
        this.tipo = tipo;
    }

    public Integer getRefCilindro() {
        return refCilindro;
    }

    public void setRefCilindro(Integer refCilindro) {
        this.refCilindro = refCilindro;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public Producto getFkProducto() {
        return fkProducto;
    }

    public void setFkProducto(Producto fkProducto) {
        this.fkProducto = fkProducto;
    }

    public Inventario getFkIdInventario() {
        return fkIdInventario;
    }

    public void setFkIdInventario(Inventario fkIdInventario) {
        this.fkIdInventario = fkIdInventario;
    }

    @XmlTransient
    public Collection<FactVen> getFactVenCollection() {
        return factVenCollection;
    }

    public void setFactVenCollection(Collection<FactVen> factVenCollection) {
        this.factVenCollection = factVenCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refCilindro != null ? refCilindro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PresentacionProducto)) {
            return false;
        }
        PresentacionProducto other = (PresentacionProducto) object;
        if ((this.refCilindro == null && other.refCilindro != null) || (this.refCilindro != null && !this.refCilindro.equals(other.refCilindro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.PresentacionProducto[ refCilindro=" + refCilindro + " ]";
    }
    
}
