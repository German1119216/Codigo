/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author GERMAN
 */
@Entity
@Table(name = "fact_ven")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FactVen.findAll", query = "SELECT f FROM FactVen f"),
    @NamedQuery(name = "FactVen.findByRefFactVen", query = "SELECT f FROM FactVen f WHERE f.refFactVen = :refFactVen"),
    @NamedQuery(name = "FactVen.findByFecha", query = "SELECT f FROM FactVen f WHERE f.fecha = :fecha")})
public class FactVen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Ref_Fact_Ven")
    private Integer refFactVen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "Pago")
    private byte[] pago;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkRefFactVen")
    private Collection<DetalleVen> detalleVenCollection;
    @JoinColumn(name = "Fk_Presentacion_Producto", referencedColumnName = "Ref_Cilindro")
    @ManyToOne(optional = false)
    private PresentacionProducto fkPresentacionProducto;
    @JoinColumn(name = "FK_Id_Emp", referencedColumnName = "C")
    @ManyToOne(optional = false)
    private Empleado fKIdEmp;
    @JoinColumn(name = "Fk_Id_Usu", referencedColumnName = "Id_Usuario")
    @ManyToOne(optional = false)
    private Usuario fkIdUsu;

    public FactVen() {
    }

    public FactVen(Integer refFactVen) {
        this.refFactVen = refFactVen;
    }

    public FactVen(Integer refFactVen, Date fecha, byte[] pago) {
        this.refFactVen = refFactVen;
        this.fecha = fecha;
        this.pago = pago;
    }

    public Integer getRefFactVen() {
        return refFactVen;
    }

    public void setRefFactVen(Integer refFactVen) {
        this.refFactVen = refFactVen;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public byte[] getPago() {
        return pago;
    }

    public void setPago(byte[] pago) {
        this.pago = pago;
    }

    @XmlTransient
    public Collection<DetalleVen> getDetalleVenCollection() {
        return detalleVenCollection;
    }

    public void setDetalleVenCollection(Collection<DetalleVen> detalleVenCollection) {
        this.detalleVenCollection = detalleVenCollection;
    }

    public PresentacionProducto getFkPresentacionProducto() {
        return fkPresentacionProducto;
    }

    public void setFkPresentacionProducto(PresentacionProducto fkPresentacionProducto) {
        this.fkPresentacionProducto = fkPresentacionProducto;
    }

    public Empleado getFKIdEmp() {
        return fKIdEmp;
    }

    public void setFKIdEmp(Empleado fKIdEmp) {
        this.fKIdEmp = fKIdEmp;
    }

    public Usuario getFkIdUsu() {
        return fkIdUsu;
    }

    public void setFkIdUsu(Usuario fkIdUsu) {
        this.fkIdUsu = fkIdUsu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (refFactVen != null ? refFactVen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactVen)) {
            return false;
        }
        FactVen other = (FactVen) object;
        if ((this.refFactVen == null && other.refFactVen != null) || (this.refFactVen != null && !this.refFactVen.equals(other.refFactVen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.FactVen[ refFactVen=" + refFactVen + " ]";
    }
    
}
